﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Model
{
    public class MNISTNumbers
    {
        [Key]
        public int mnist_numbers_Id { get; set; }

        [MaxLength(255)]
        public int mnist_class_name_id { get; set; }

        [MaxLength(255)]
        public String mnist_training_data_set { get; set; }
    }
}
