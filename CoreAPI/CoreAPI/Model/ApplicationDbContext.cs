﻿using Microsoft.EntityFrameworkCore;

namespace CoreAPI.Model
{
    public class ApplicationDbContext :DbContext
    {
        public DbSet<MNISTClass> MNISTClass { get; set; }
        public DbSet<MNISTNumbers> MNISTNumbers { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

            optionsBuilder.UseSqlite("Data Source=MNISTData.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
