﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CoreAPI.Model
{
    public class MNISTClass
    {
        [Key]
        public int mnist_class_id { get; set; }

        [MaxLength(255)]
        public int mnist_class_name_id { get; set; }

        [MaxLength(255)]
        public String mnist_class_name { get; set; }
    }
}