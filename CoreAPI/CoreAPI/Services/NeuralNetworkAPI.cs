﻿using System.Threading.Tasks;
using System.Net.Http;
using CoreAPI.Model;
using System.Collections.Generic;

namespace CoreAPI.Services
{
    public class NeuralNetworkAPI
    {
        public async Task<string> GetNeuralNetworkVersion()
        {
            var HttpClient = new HttpClient();
            var response = await HttpClient.GetAsync("http://localhost:3500/");
            var content = await response.Content.ReadAsStringAsync();
            return content;
        }

        public async Task<int> trainNeuralNetwork(List<MNISTNumbers> query)
        {
            var HttpClient = new HttpClient();
            var response = await HttpClient.PostAsJsonAsync("http://localhost:3500/learn", query);

            return 1;
        }
            
        public async Task<string> getPrediction(string image_data)
        {
            var HttpClient = new HttpClient();
            var response = await HttpClient.PostAsJsonAsync("http://localhost:3500/query", image_data);
            var content = await response.Content.ReadAsStringAsync();
            return content;
        }
    }
}
