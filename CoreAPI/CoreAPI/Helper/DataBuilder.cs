﻿using CoreAPI.Model;
using System;
using System.IO;

namespace CoreAPI.Helper
{
    public class DataBuilder
    {
        private enum MNISTDataClasses { Zero,One,Two,Three,Four,Five,Six,Seven,Eight,Nine };
  
        private const String rootFolder = "./NeuralNetworkData/";
        private ApplicationDbContext applicationDbContext;

        public DataBuilder(ApplicationDbContext _applicationDbContext)
        {
            applicationDbContext = _applicationDbContext;
        }

        public void LoadTrainingData(String file)
        { 
        
            using (StreamReader r = new StreamReader(rootFolder + file))
            {
                int classId;
                String trainingSet;
                while ((trainingSet = r.ReadLine()) != null)
                {
                    classId = Int32.Parse(trainingSet.Substring(0,1));
                    applicationDbContext.MNISTNumbers.Add(new MNISTNumbers
                    {
                        mnist_class_name_id = classId,
                        mnist_training_data_set = trainingSet.Remove(0, 2)
                    });
                }

                applicationDbContext.SaveChanges();
            }

            for(int i = 0; i < 10; i++)
            {
                applicationDbContext.MNISTClass.Add(new MNISTClass
                {
                    mnist_class_name_id = i,
                    mnist_class_name = getClassName(i)
                });
            }

            applicationDbContext.SaveChanges();
        }

        public String getClassName(int classId)
        {
            switch (classId)
            {
                case (int) MNISTDataClasses.Zero:
                    return "Zero";

                case (int)MNISTDataClasses.One:
                    return "One";

                case (int)MNISTDataClasses.Two:
                    return "Two";

                case (int)MNISTDataClasses.Three:
                    return "Three";

                case (int)MNISTDataClasses.Four:
                    return "Four";

                case (int)MNISTDataClasses.Five:
                    return "Five";

                case (int)MNISTDataClasses.Six:
                    return "Six";

                case (int)MNISTDataClasses.Seven:
                    return "Seven";

                case (int)MNISTDataClasses.Eight:
                    return "Eight";

                case (int)MNISTDataClasses.Nine:
                    return "Nine";

                default:
                    return "N/A";
            }
        }

    }
}
