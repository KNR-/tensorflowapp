﻿using CoreAPI.Model;
using CoreAPI.Services;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CoreAPI.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("CorsPolicy")]
    [ApiController]
    public class NeuralNetworkController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private NeuralNetworkAPI neuralNetworkAPI = new NeuralNetworkAPI();

        public NeuralNetworkController(ApplicationDbContext context)
        {
            _context = context;
        }

        // Get api/neuralnetwork
        [HttpGet]
        [Produces("application/json")]
        public async Task<IActionResult> GetNeuralNetworkData()
        {
            var content = await neuralNetworkAPI.GetNeuralNetworkVersion();
            JObject json = JObject.Parse(content);
            return Ok(json);
        }

        // Post api/neuralnetwork
        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> TrainNeuralNetwork()
        {
            using (_context)
            {         
                var query = _context.MNISTNumbers.ToList();

                if(!query.Any())
                {
                    return NotFound();
                }
                var content = await neuralNetworkAPI.trainNeuralNetwork(query);
                return Ok();
            }
        }

        // Put api/neuralnetwork
        [HttpPut]
        [Produces("application/json")]
        public async Task<IActionResult> SendImage([FromBody] SentImage sentImage)
        {
            var content = await neuralNetworkAPI.getPrediction(sentImage.image_array);
            JObject json = JObject.Parse(content);
            return Ok(json);
        }
    }    

}