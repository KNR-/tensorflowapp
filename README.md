# TensorFlowApp

## Run Python Flask Server 
```
python server.py
url: localhost:3500
```

## Build  Database & Run Dotnet Core API
```
dotnet ef migrations add InitialCreate
dotnet ef database update
dotnet watch run
url: localhost:5000
```
## TensorFlow App
```
npm install
ng serve
url: localhost:4200
```