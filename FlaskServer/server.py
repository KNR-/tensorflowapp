from flask import Flask
from flask import json
from flask import request

import home
from neuralnetwork import tensor_flow_active,teach_neuralnetwork, query

app = Flask(__name__)

@app.route("/")
def homeRoute():
    response = app.response_class(
        response=json.dumps({"tensorflow_version": tensor_flow_active()}),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route("/learn", methods=['POST'])
def learnRoute():
    teach_neuralnetwork(request.json)
    response = app.response_class(
        response=json.dumps(request.json),
        status=200,
        mimetype='application/json'
    )
    return response   

@app.route("/query", methods=['POST'])
def queryRoute():
    response = app.response_class(
        response=json.dumps({'prediction': query(request.json)}),
        status=200,
        mimetype='application/json'
    )
    return response   

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=3500, debug=True)