from __future__ import absolute_import, division, print_function

# TensorFlow and tf.keras
import tensorflow as tf
config = tf.ConfigProto()
config.gpu_options.allow_growth = True
sess = tf.Session(config=config)

from tensorflow import keras
from tensorflow.python.keras.models import Sequential
from tensorflow.python.keras.layers import Dense, Flatten, Conv2D, Dropout, MaxPooling2D

# Image width / height
size = 28

# Image max brigthness value
image_max_value = 255

# Classification count => Numbers from 0 - 9
classes_count = 10

# Helper libraries
import numpy as np

def tensor_flow_active():
    return tf.__version__

def teach_neuralnetwork(training_data):
    training_data = np.array(training_data)
    x_train = np.empty((len(training_data), size,size,1))
    y_train = np.empty((len(training_data)))

    # Shape data to neural network
    for i in range(len(x_train)):
        x_train[i] = np.array(training_data[i]['mnist_training_data_set'].split(',')).reshape(size,size,1)
        y_train[i] = np.array(training_data[i]['mnist_class_name_id'])

    x_train = x_train / image_max_value
    y_train = tf.keras.utils.to_categorical(y_train, classes_count)

    # Create Model
    model = Sequential()
    model.add(Conv2D(32,kernel_size=(3,3),activation='relu',kernel_initializer='he_normal',input_shape=(size,size,1)))
    model.add(MaxPooling2D((2,2)))
    model.add(Conv2D(64,kernel_size=(3,3),activation='relu'))
    model.add(MaxPooling2D(pool_size=(2,2)))
    model.add(Conv2D(128,kernel_size=(3,3),activation='relu'))
    model.add(Flatten())
    model.add(Dense(128,activation="relu"))
    model.add(Dense(10,activation='softmax'))

    # Compile model
    model.compile(
            loss='categorical_crossentropy',
            optimizer='adam',
            metrics=['accuracy'])

    # Train model
    model.fit(x_train,y_train,batch_size=64,epochs=5)
    
    # Save model & weights
    model.save('mnist_model.h5')
    model.save_weights('mnist_weights.h5')

    return 1

def query(image):

    # Parse image
    image = np.array(image.split(',')).reshape(size,size,1)

    # Load model & weights
    model = keras.models.load_model('mnist_model.h5')
    model.load_weights('mnist_weights.h5')

    # Make a prediction to model
    prediction = int(model.predict_classes(np.array([image]))[0])

    print("Query Executed")
    return prediction


