import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CoreApiService {

  APIAddress = "";

  constructor(private http: HttpClient) {
    this.APIAddress = "localhost:5000";
  }

  sendImage(imageArray) {
    return this.http.put("http://" + this.APIAddress +"/api/neuralnetwork",imageArray);
  }

  trainNeuralNetwork() {
    return this.http.post("http://" + this.APIAddress +"/api/neuralnetwork",'');
  }
}
