import { Component, OnInit, ViewChild, ElementRef, Inject } from '@angular/core';

import { DOCUMENT } from '@angular/common';
import { WINDOW } from "../../../core/window.service";

import { CoreApiService } from 'src/app/core-api.service';

@Component({
  selector: 'app-draw-image',
  templateUrl: './draw-image.component.html',
  styleUrls: ['./draw-image.component.scss']
})
export class DrawImageComponent implements OnInit {

  @ViewChild('canvas') canvas: ElementRef;
  @ViewChild('container') container: ElementRef;

  mousePosX 		= 0;
  mousePosY 		= 0;

  offsetTopContainer 	= 125;
  offsetLeftContainer = 0;

  mousePressed = false;
  pencilRadius = 1;

  predictionResult;
  predictionFetched = false;
  predictionFetching = false;

  trainingNeuralNetwork = false;

  maxHistoryElements = 5;
  paintedHistory = [];

  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject(WINDOW) private window: Window,
    private coreApiService: CoreApiService
  ) { }

  ngOnInit() {
   
  }

  drawToCanvas(xPos,yPox,radius) {
    let canvas  = this.canvas.nativeElement;
    let ctx     = canvas.getContext('2d');
    
    ctx.arc(xPos,yPox,radius,0, 2 * Math.PI, false);
    ctx.moveTo(xPos,yPox);
    ctx.fill();

  }

  _onMouseUp(event) {
    this.mousePressed = false;
    
  }
  _onMouseDown(event) {
    this.mousePressed = true;
  }

  _onMouseMove(event) { 
    event.preventDefault();
    
    if(!this.mousePressed) {
      return;
    }

    let viewContainer = this.container.nativeElement;
    let offsetLeft 		= viewContainer.offsetLeft;
    let offsetTop 		= viewContainer.offsetTop;

    let mouseX 				= event.clientX - offsetLeft - this.canvas.nativeElement.offsetLeft - this.offsetLeftContainer + window.pageXOffset;
    let mouseY 				= event.clientY - offsetTop + window.scrollY - this.offsetTopContainer;

    this.mousePosX = this.normaliseValue(mouseX,this.canvas.nativeElement.scrollWidth,0,this.canvas.nativeElement.width,0);
    this.mousePosY = this.normaliseValue(mouseY,this.canvas.nativeElement.scrollHeight,0,this.canvas.nativeElement.height,0);

    this.drawToCanvas(Math.round(this.mousePosX),Math.round(this.mousePosY),this.pencilRadius)
  }

  _sendImage(event) {
    let ctx     = this.canvas.nativeElement.getContext('2d');
    let height  = this.canvas.nativeElement.height;
    let width   = this.canvas.nativeElement.width;

    // Get image data from canvas as RBGA
    let imgSrc  = ctx.getImageData(0,0,width,height).data;

    let canvas = this.canvas.nativeElement as HTMLCanvasElement
      
    this.handleHistoryArray(canvas.toDataURL())

    // Image data stored in this array
    let image_array = "";

    for(var i = 0; i < imgSrc.length; i += 4) {
      image_array += imgSrc[i + 3] + ",";
    }

    // Remove last dot from array
    let sentImage = {
      "image_array": image_array.substr(0,image_array.length -1)
    }

    this.predictionFetched = false;
    this.predictionFetching = true;
    
    this.coreApiService.sendImage(sentImage)
    .subscribe(result => {
      this.predictionResult = result['prediction'];
      this.predictionFetched = true;
      this.predictionFetching = false;
    }),(error) => {
      console.log(error);
    };
  }


  _trainNeuralNetwork() {

    this.trainingNeuralNetwork = true;

    this.coreApiService.trainNeuralNetwork()
    .subscribe(result => {
      this.trainingNeuralNetwork = false;
    }),(error) => {
      console.log(error);
    };
  }

  _clearCanvas() {

    this.predictionFetched = false;
    
    let ctx = this.canvas.nativeElement.getContext('2d');
    let height  = this.canvas.nativeElement.height;
    let width   = this.canvas.nativeElement.width;

    ctx.clearRect(0,0,width,height)
    ctx.beginPath();

  }

  normaliseValue(inputValue,maxValue,minValue,maxRange,minRange) {
    return (((inputValue - (minValue - 0.01)) * (maxRange - (minRange))) / ((maxValue - 0.01) - (minValue - 0.01))) + (minRange);
  }


  handleHistoryArray(dataUrl) {

    if(this.paintedHistory.length >= this.maxHistoryElements) {
      this.paintedHistory.pop();
      this.paintedHistory.unshift(dataUrl);
    } else {
      this.paintedHistory.unshift(dataUrl);
    }
  }

}
