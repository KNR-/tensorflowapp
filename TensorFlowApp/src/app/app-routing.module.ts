import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DrawImageComponent } from './components/draw-image/draw-image.component';
import { AppContainerComponent} from './app-container/app-container.component'

const routes: Routes = [
  {path: '', component: AppContainerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
