import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DrawImageComponent } from './components/draw-image/draw-image.component';

import { WINDOW_PROVIDERS } from "../core/window.service";
import { CoreApiService } from 'src/app/core-api.service';

import { HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';
import { AppContainerComponent } from './app-container/app-container.component';

@NgModule({
  declarations: [
    AppComponent,
    DrawImageComponent,
    NavbarComponent,
    AppContainerComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule
  ],
  providers: [WINDOW_PROVIDERS,CoreApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
